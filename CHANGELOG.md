# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.2.1...v1.3.0) (2021-07-02)


### Features

* **HistoryManager:** Add update_batch method ([f084e6b](https://gitlab.com/SyntheticGoop/timeshadow/commit/f084e6b49b51e96921c24750447d59a0f7a65c00))

### [1.2.1](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.2.0...v1.2.1) (2021-06-30)


### Bug Fixes

* **Timeshadow:** last and first should create source from snapshot ([d217682](https://gitlab.com/SyntheticGoop/timeshadow/commit/d2176826bbdecdc2d9d2f4a32fc6213e5c1857a4))

## [1.2.0](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.1.1...v1.2.0) (2021-06-30)


### Features

* **Timeshadow:** Add the ability to get shadow from first and last ([c918363](https://gitlab.com/SyntheticGoop/timeshadow/commit/c918363609aedb1e023e7dcaad9192c3b19e70f9))

### [1.1.1](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.1.0...v1.1.1) (2021-05-04)


### Bug Fixes

* **Timeshadow:** Multiple invoked lazy_shadow from the same getted property should point to the same data ([a73e780](https://gitlab.com/SyntheticGoop/timeshadow/commit/a73e78039abb44311b8c52373587653b3d78bc19))

## [1.1.0](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.5...v1.1.0) (2021-05-04)


### Features

* **Timeshadow:** Add lazily invoked shadow snapshot ([ec4d716](https://gitlab.com/SyntheticGoop/timeshadow/commit/ec4d716d58791c35c6b73073213cbf0f82e16394))

### [1.0.5](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.4...v1.0.5) (2021-02-08)


### Bug Fixes

* Remmutalized data should not cache prototype ([02d2999](https://gitlab.com/SyntheticGoop/timeshadow/commit/02d299985d2e629370439617c81d71d068b67b98))

### [1.0.4](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.3...v1.0.4) (2021-02-06)

### [1.0.3](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.2...v1.0.3) (2021-01-31)

### [1.0.2](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.1...v1.0.2) (2021-01-23)


### Bug Fixes

* **HistoryManager:** Update now restores history correctly. ([bae80f2](https://gitlab.com/SyntheticGoop/timeshadow/commit/bae80f2062d96bafcb7537f38c8e045b11fd0d30))

### [1.0.1](https://gitlab.com/SyntheticGoop/timeshadow/compare/v1.0.0...v1.0.1) (2021-01-20)


### Bug Fixes

* Fix examples ([abbc08c](https://gitlab.com/SyntheticGoop/timeshadow/commit/abbc08c1e1bcca85a4f0446a687fc561bd489c6c))

## 1.0.0 (2021-01-20)


### ⚠ BREAKING CHANGES

* Timeshadow construction requires injection of delta algorithm

### Features

* Modify API to properly handle initial data diff. ([bffbbfe](https://gitlab.com/SyntheticGoop/timeshadow/commit/bffbbfed7e001786d427932aabef0a6a797df650))
* **HistoryManager:** Add update from Delta function. ([4e0bb2c](https://gitlab.com/SyntheticGoop/timeshadow/commit/4e0bb2cc99034058ee59ab20877b333f3b948059))
* Add dry commits ([7a48bc9](https://gitlab.com/SyntheticGoop/timeshadow/commit/7a48bc9e8d9920f1de5183d1e4849e7cfcce79cc))
* Add root to Shadow, fix tests ([7846873](https://gitlab.com/SyntheticGoop/timeshadow/commit/7846873659d33d0d9fbfab60b9cfe204a5d09a71))
* Add snapshot promotion ([2b56235](https://gitlab.com/SyntheticGoop/timeshadow/commit/2b5623527be2a87a6cc68fe3d9d4705b13e96e3a))
* Change filter to last and first for chronological searching ([5ec47d4](https://gitlab.com/SyntheticGoop/timeshadow/commit/5ec47d4ea460d9ac55fd16532e84053fa79d6591))
* Commit returns delta ([b7f0c0a](https://gitlab.com/SyntheticGoop/timeshadow/commit/b7f0c0ad08c40bc0860dd950dd91e5f2ece987c7))
* Expose History ([c0a7b9a](https://gitlab.com/SyntheticGoop/timeshadow/commit/c0a7b9a2cdce67c1a13d688aa0614eaa599aa93e))
* Force delta algorithms to be injected at runtime ([bea95db](https://gitlab.com/SyntheticGoop/timeshadow/commit/bea95dbc2752cff2599fb4ee6e45de337c35d76e))
* **delta-difflog:** Create the DeltaDifflog library. ([49d05df](https://gitlab.com/SyntheticGoop/timeshadow/commit/49d05df9fedfd747c7e105809c743ce433f54ea9))
* **DeltaDifflog:** Add load_changlog function ([db24b18](https://gitlab.com/SyntheticGoop/timeshadow/commit/db24b18f77cd167f24010a52015648d746b21f9c))
* **DeltaDifflog:** Add static new function ([dd545c0](https://gitlab.com/SyntheticGoop/timeshadow/commit/dd545c04e61e357e732eed2efe429c43b162093c))
* **migrate:** Deprecate DeltaDifflog and migrate to Timeshadow ([ae7f6b0](https://gitlab.com/SyntheticGoop/timeshadow/commit/ae7f6b0fadcb5294bc5c26cd062b6b6527561f57))


### Bug Fixes

* Output both esm and cjs ([8308ee7](https://gitlab.com/SyntheticGoop/timeshadow/commit/8308ee75250639e430b23ec7b4aabbd3f6e04a46))
* Return root when root is called instead of source ([62d8681](https://gitlab.com/SyntheticGoop/timeshadow/commit/62d86819b50ecdd0794797c1d03d3c0a391e1958))
* Set type field in package.json ([76deeb7](https://gitlab.com/SyntheticGoop/timeshadow/commit/76deeb77d76b492f9485456db1140097e49b8df9))
