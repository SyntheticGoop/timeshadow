# Timeshadow allows you to version and transverse object histories

## Usage

Timeshadow always needs to be initialized with data. It cannot be blank at the start.

In order to mutate the data, the `.shadow` property will create an independent mutable view of the data.
This view has to then be commited for the change to reflect in the main object.
After the change is commited from one view, all other shadows' `.conflict` property will return true except for the current one.

### Initialization

```ts
const DATA = { data: 'some data', type: 'string' }
const META = Date.now()


import { BasicDiff, Timeshadow } from '../main'

let ts = new Timeshadow<typeof DATA, typeof META>(BasicDiff)

/* Clone existing delta */
ts = Timeshadow.new(BasicDiff, ts)

/* Using delta */
ts = new Timeshadow(BasicDiff, ts.delta)
ts = Timeshadow.new(BasicDiff, { delta: ts.delta })

/* A static `new` helper is available */
ts = Timeshadow.new(BasicDiff, ts)
```

### Adding data

```ts
/* Adding new complete object */
const NEW_META = Date.now()
const s = ts.shadow

/* Direct mutation */
s.data = { data: 'new data', type: 'number' }
s.data.type ='number'
s.meta = NEW_META
s.commit()

/* Directly replacing */
s.replace({ data: 'new data', type: 'number' }, NEW_META)
/* Forcing an update even though the data is the same */
s.replace(DATA, NEW_META, true)

/* Adding parts of an object if applicable. */
s.partial({ type: 'number' }, NEW_META)
/* Forcing an update even though the data is the same */
s.partial({ type: 'string' }, NEW_META, true)

/* With a function */
s.modify(({ data, type }) => ({ data: `1: ${data}`, type }), NEW_META)
/* Forcing an update even though the data is the same */
s.modify(({ data, type }) => ({ data, type }), NEW_META, true)

/* While chaining multiple */
s
  .replace({ data: 'new data', type: 'number' }, NEW_META)
  .partial({ data: 'new data' }, NEW_META, true)
  .modify(({ data, type }) => ({ data, type: 'string' }), NEW_META)
  /* You can also chain a commit at the end */
  .commit()
```

### Using with multiple shadows

```ts
/* With multiple shadows */
const s1 = ts.shadow
const s2 = ts.shadow

s1.conflict === false
s2.conflict === false

s1.data = { data: 'new data', type: 'number' }
s2.data = { data: 'new data', type: 'number' }


s1.conflict === false
s2.conflict === false

s1.commit()
s1.conflict === false
s2.conflict === true

s2.commit()
s1.conflict === true
s2.conflict === false
```

### Accessing data

```ts
/* These properties are recursively readonly. Mutating them will throw an error */
ts.data // Current data value
ts.meta // Current meta value
ts.full // Expands the all delta diffs into original values
ts.delta // All delta diffs
ts.changelog // Current data, meta and delta diffs. This is for serializing.


/* You can query a particular change in history from the start */
ts.first(m => m == META) // returns
ts.first(m => m == -1 ) // Throws
ts.first(m => m == -1, false ) // undefined
/* Or the end */
ts.last(m => m < Date.now())

/*
If you need anything more, it's better to just generate everything.
This will generate a new array each time, so don't recklessly call it repeatedly
*/
ts.full.filter(x => x.meta > Date.now())
```
