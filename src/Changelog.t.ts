import type { Delta } from 'jsondiffpatch'


type Primitive = null | boolean | number | string


export type Coredata = undefined | Primitive | Coredata[] | { [i: string]: Coredata }



export type DefinedCoredata = Primitive | Coredata[] | { [i: string]: Coredata }



export type ChangeList<Data extends Coredata, Meta> = [/* Index: number,  */Data: Data, Meta: Meta]



export type Changelog<Data extends Coredata, Meta> = {
  data: Data
  meta: Meta
}

export type DeltaList = [Delta, ...Delta[]]

export function is_delta_list(delta: DeltaList | Delta[]): delta is DeltaList {
  return delta.length > 0
}

export type Difflog<Data extends Coredata, Meta> = Changelog<Data, Meta> & {
  delta: DeltaList
}



export type DiffDelta<Data extends Coredata, Meta> = [/* Index: number, */ Data: Data, Meta: Meta]