import 'ts-jest'
import { ArrayLCSDiff, BasicDiff } from './DeltaTool'


describe.each([
  ['ArrayLCS', ArrayLCSDiff],
  ['Basic', BasicDiff]
])('tests %s', (_, d) => {
  it('Creates delta from scratch', () => expect(d.create([{ a: 1, b: 2 }, { c: 3, d: 4 }])).toMatchSnapshot())

  it('Creates delta with base', () => expect(d.create([{ a: 1, b: 2 }, { c: 3, d: 4 }], [{ e: 5, f: 6 }, { g: 7, d: 8 }])).toMatchSnapshot())

  it('Restores delta from scratch', () => expect(d.revert(d.create([{ a: 1, b: 2 }, { c: 3, d: 4 }])!)).toMatchObject([{ a: 1, b: 2 }, { c: 3, d: 4 }]))

  it('Restores delta with base', () => expect([...d.revert(d.create([{ a: 1, b: 2 }, { c: 3, d: 4 }], [{ e: 5, f: 6 }, { g: 7, d: 8 }])!, [{ e: 5, f: 6 }, { g: 7, d: 8 }])]).toMatchObject([{ a: 1, b: 2 }, { c: 3, d: 4 }]))
})