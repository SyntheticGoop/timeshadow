import { remutalize } from 'immutalize';
import type { Delta } from 'jsondiffpatch';
import { create } from 'jsondiffpatch';
import hash from 'object-hash';
import type { ChangeList, Coredata, DefinedCoredata } from './Changelog.t';

export interface DeltaTool<Data extends Coredata, Meta extends DefinedCoredata> {
  tool: any
  /**
   * Generates a delta.
   * 
   * @param next Next data.
   * @param base Base data to delta over.
   */
  create(next: ChangeList<Data, Meta>, base?: ChangeList<Data, Meta> | undefined): Delta | undefined
  /**
   * Restores a delta.
   * 
   * @param changes Delta to restore.
   * @param base Base to restore delta off.
   */
  revert(changes: Delta, base?: ChangeList<Data, Meta> | undefined): ChangeList<Data, Meta>
}



export const ArrayLCSDiff: DeltaTool<any, any> = {
  tool: create({ objectHash: hash }),
  create(next, base?) {
    if (!base) return this.tool.diff(undefined, next);
    return this.tool.diff(base, next);
  },

  revert(changes, base?) {
    if (!base) return this.tool.patch(undefined, changes);
    return this.tool.patch(remutalize(base), changes);
  }
}


export const BasicDiff: DeltaTool<any, any> = {
  tool: create(),
  create(next, base?) {
    if (!base) return this.tool.diff(undefined, next);
    return this.tool.diff(base, next);
  },
  revert(changes, base?) {
    if (!base) return this.tool.patch(undefined, changes);
    return this.tool.patch(remutalize(base), changes);
  }
}