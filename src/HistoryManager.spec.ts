import 'ts-jest'
import type { DeltaList } from './Changelog.t'
import { BasicDiff } from './DeltaTool'
import { HistoryManager } from './HistoryManager'
const d = BasicDiff

describe('tests', () => {
  const datamodel = [
    { data: { a: 1, b: [2, { c: 3 }] }, meta: { x: 1, a: 1, b: [2, { c: 3 }] } },
    { data: { a: 4, b: [2, { c: 3 }] }, meta: { x: 2, a: 1, b: [5, { c: 3 }] } },
    { data: { a: 4, n: [2, { c: 4 }] }, meta: { x: 3, o: 1, b: [2, { c: 1 }] } },
    { data: { a: 2, n: [2, { c: 3 }] }, meta: { x: 4, o: 1, u: [2, { e: 3 }] } },
    { data: { a: 2, e: [1, { c: 3 }] }, meta: { x: 5, i: 1, u: [2, { e: 3 }] } },
  ]

  const delta_list: DeltaList = [
    d.create([datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[1].data, datamodel[1].meta], [datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[2].data, datamodel[2].meta], [datamodel[1].data, datamodel[1].meta])!,
    d.create([datamodel[3].data, datamodel[3].meta], [datamodel[2].data, datamodel[2].meta])!,
    d.create([datamodel[4].data, datamodel[4].meta], [datamodel[3].data, datamodel[3].meta])!,
  ]


  it('Correctly jumps to history', () => {
    const h = HistoryManager.new<any, { x: number }>(d, delta_list)
    function datameta(x: any) {
      return { data: x.data, meta: x.meta }
    }
    expect(datameta(h.first(m => m.x === 1))).toEqual(datamodel.find(x => x.meta.x === 1))
    expect(datameta(h.first(m => m.x === 2))).toEqual(datamodel.find(x => x.meta.x === 2))
    expect(datameta(h.first(m => m.x === 3))).toEqual(datamodel.find(x => x.meta.x === 3))
    expect(datameta(h.first(m => m.x === 4))).toEqual(datamodel.find(x => x.meta.x === 4))
    expect(datameta(h.first(m => m.x === 5))).toEqual(datamodel.find(x => x.meta.x === 5))
    expect(datameta(h.last(m => (m as any)['o'] === 1))).toEqual(datamodel.find(x => x.meta.x === 4))
    expect(datameta(h.snapshot(5))).toEqual(datamodel.find(x => x.meta.x === 5))
    expect(datameta(h.snapshot(4))).toEqual(datamodel.find(x => x.meta.x === 4))
    expect(datameta(h.snapshot(3))).toEqual(datamodel.find(x => x.meta.x === 3))
    expect(datameta(h.snapshot(2))).toEqual(datamodel.find(x => x.meta.x === 2))
    expect(datameta(h.snapshot(1))).toEqual(datamodel.find(x => x.meta.x === 1))
  })


  it('Correctly copies history', () => {
    const h = HistoryManager.new(d, delta_list)
    expect(h.delta).toEqual(delta_list)
    expect(h.data).toEqual(datamodel[4].data)
    expect(h.meta).toEqual(datamodel[4].meta)
  })

  it('Adds new history', () => {
    const h = HistoryManager.new<any, { x: number }>(d, delta_list.slice(0, 3) as DeltaList)
    h.push(datamodel[3])
    h.push(datamodel[4])
    expect(h.delta).toEqual(delta_list)
  })

  it('Creates new history from data', () => {
    const h = HistoryManager.init<any, { x: number }>(d)
    expect(() => h.delta).toThrow()
  })

  it('Updates history to current', () => {
    const h = HistoryManager.init<any, { x: number }>(d)
    for (const d of delta_list) {
      h.update(d)
    }
    expect(h.delta).toEqual(delta_list)
  })

  it('Updates history to current', () => {
    const h = HistoryManager.init<any, { x: number }>(d)
    h.update_batch(delta_list)
    expect(h.delta).toEqual(delta_list)
  })
})