import { immutalize, remutalize } from 'immutalize';
import type { Delta } from 'jsondiffpatch';
import LRU from 'lru-cache';
import type { Changelog, Coredata, DefinedCoredata, DeltaList, Difflog } from './Changelog.t';
import type { DeltaTool } from './DeltaTool';
import { crush_deltas, transverse, transverse_last } from './transverse';




type History<Data extends Coredata, Meta extends DefinedCoredata> = Changelog<Data, Meta> & { delta: [{ delta: Delta, meta: Meta }, ...Array<{ delta: Delta, meta: Meta }>] }


class HistoryCore<Data extends Coredata, Meta extends DefinedCoredata> implements Difflog<Data, Meta> {
  public history: History<Data, Meta> | undefined = undefined

  private cache


  /**
   * Create new HistoryManager class
   * 
   * @param delta_tool `DeltaTool` differ
   * @param history History to use.
   * @param cache LRU cache size.
   */
  static new<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, history: DeltaList | Delta[], cache: number = 5) {
    return new HistoryCore<Data, Meta>(delta_tool, history, cache)
  }



  /**
   * Create new HistoryManager class from initial data.
   * 
   * @param delta_tool `DeltaTool` differ
   * @param data Data
   * @param meta Meta
   * @param cache LRU cache size.
   */
  static init<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, cache: number = 5) {
    return new HistoryCore<Data, Meta>(delta_tool, [], cache)
  }



  /**
   * Create new HistoryManager class
   * 
   * @param delta_tool `DeltaTool` differ
   * @param history History to use.
   * @param cache LRU cache size.
   */
  constructor(private delta_tool: DeltaTool<Data, Meta>, history: DeltaList | Delta[], cache: number) {
    const lru = new LRU<number, History<Data, Meta>>({ max: cache })
    this.cache = lru

    if (history.length === 0) return

    const deltas = transverse<Data, Meta>(delta_tool, history)

    const first = deltas.next()
    if (first.done) throw RangeError('History is empty')

    this.history = {
      data: first.value.data,
      meta: first.value.meta,
      delta: [{ delta: first.value.delta, meta: first.value.meta }]
    }

    for (const { delta, data, meta } of deltas) {
      this.history.delta.push({ delta, meta })
      this.history.data = data
      this.history.meta = meta
    }
    this.cache.set(this.history.delta.length, { data: this.history.data, meta: this.history.meta, delta: [...this.history.delta] })
  }


  /**
   * History snapshot.
   * 
   * @param index Index of history to get.
   * @returns History snapshot.
   */
  get(index: number): History<Data, Meta> {
    if (!this.history) throw Error('No history')

    const cached = this.cache.get(index)
    if (cached) {
      return { data: remutalize(cached.data as any), meta: remutalize(cached.meta as any), delta: [...cached.delta] }
    }

    const closest = this.cache.keys().sort().reverse().find(i => i <= index)
    const base = closest != undefined ? this.cache.get(closest) : undefined

    const slice = (closest == undefined ? this.history.delta.slice(0, index) : this.history.delta.slice(closest, index)) as History<Data, Meta>['delta']

    const get = transverse_last<Data, Meta>(this.delta_tool, slice.map(x => x.delta), base)

    this.cache.set(index, { ...get, delta: [...slice] })

    return { data: remutalize(get.data as any), meta: remutalize(get.meta as any), delta: [...slice] }
  }


  set(history: History<Data, Meta> | undefined) {
    this.history = history ? {
      data: immutalize(history.data as any),
      meta: immutalize(history.meta as any),
      delta: history.delta.map(immutalize) as typeof history.delta,
    } : history
  }



  /**
   * Push new data to history.
   * 
   * @param data Data to add to history.
   */
  push(data: { data: Data, meta: Meta }) {
    const delta = this.delta_tool.create([data.data, data.meta], this.history ? [this.history.data, this.history.meta] : undefined)

    if (delta) {
      if (this.history) {
        this.history.delta.push({
          delta,
          meta: immutalize(data.meta as any)
        })
        this.history.data = immutalize(data.data as any)
        this.history.meta = immutalize(data.meta as any)
      } else {
        this.history = {
          meta: immutalize(data.meta as any),
          data: immutalize(data.data as any),
          delta: [{ delta, meta: immutalize(data.meta as any) }],
        }
      }
    }

    return delta
  }

  /**
   * Updates the core history with new delta data.
   * 
   * @param delta Change to update history with.
   */
  public update(delta: Delta) {
    const deltas = transverse<Data, Meta>(this.delta_tool, [delta], this.history)

    const first = deltas.next()
    if (first.done) throw RangeError('History is empty')

    if (this.history) {
      this.history = {
        data: first.value.data,
        meta: first.value.meta,
        delta: [
          ...this.history.delta,
          {
            delta: first.value.delta,
            meta: first.value.meta,
          }
        ]
      }
    } else {
      this.history = {
        data: first.value.data,
        meta: first.value.meta,
        delta: [{ delta: first.value.delta, meta: first.value.meta }]
      }
    }
  }

  /**
   * Updates the core history with list of new delta data.
   * 
   * @param delta Changes to update history with.
   */
  public update_batch(delta: DeltaList) {
    const { active, history } = crush_deltas<Data, Meta>(this.delta_tool, delta, this.history)

    if (this.history) {
      this.history = {
        data: active.data,
        meta: active.meta,
        delta: [
          ...this.history.delta,
          ...history,
          {
            delta: active.delta,
            meta: active.meta,
          }
        ]
      }
    } else {
      this.history = {
        data: active.data,
        meta: active.meta,
        delta: [
          ...history,
          { delta: active.delta, meta: active.meta }
        ] as History<Data, Meta>['delta']
      }
    }
  }

  /**
   * Generates push result without pushing.
   * 
   * @param data Data to add to history.
   */
  dry_push(data: { data: Data, meta: Meta }) {
    return this.delta_tool.create([data.data, data.meta], this.history ? [this.history.data, this.history.meta] : undefined)
  }



  /**
   * Project a HistoryCore view.
   * 
   * @param index Index of history to get.
   */
  view(index: number) {
    const history = this.get(index)

    const obj = this

    const view: HistoryCore<Data, Meta> = new Proxy(
      obj,
      {
        get(target, prop: keyof typeof obj) {
          switch (prop) {
            case 'history':
              return history
            case 'get':
              return target.get.bind(view)
            case 'view':
              return target.view.bind(view)
            case 'data':
              return history.data
            case 'meta':
              return history.meta
            case 'delta':
              return history.delta.map(x => x.delta)
            case 'set':
              return target.set.bind(view)
            case 'revision':
              return index
          }

          return target[prop]
        }
      }
    )
    return view
  }

  get has_history() {
    return !!this.history
  }

  get data() {
    if (!this.history) throw Error('No data')
    return this.history.data
  }

  get meta() {
    if (!this.history) throw Error('No meta')
    return this.history.meta
  }

  get delta() {
    if (!this.history) throw Error('No delta')
    return this.history.delta.map(x => x.delta) as DeltaList
  }

  get changelog(): Difflog<Data, Meta> {
    return {
      data: this.data,
      meta: this.meta,
      delta: this.delta,
    }
  }

  get revision() {
    return this.history?.delta.length ?? 0
  }
}


type HistoryInstance<Data extends Coredata, Meta extends DefinedCoredata> = HistoryManager<Data, Meta> & {
  /** Make the current snapshot the parent. */
  promote: () => HistoryManager<Data, Meta>
}


/**
 * Transverse history of delta.
 */
export class HistoryManager<Data extends Coredata, Meta extends DefinedCoredata> implements Difflog<Data, Meta> {
  /**
   * Create new HistoryManager class
   * 
   * @param delta_tool `DeltaTool` differ
   * @param history History to use.
   * @param cache LRU cache size.
   */
  static new<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, history: DeltaList, cache: number = 5) {
    return new HistoryManager<Data, Meta>(HistoryCore.new(delta_tool, history, cache))
  }



  /**
   * Create new HistoryManager class from initial data.
   * 
   * @param delta_tool `DeltaTool` differ
   * @param data Data
   * @param meta Meta
   * @param cache LRU cache size.
   */
  static init<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, cache: number = 5) {
    return new HistoryManager<Data, Meta>(HistoryCore.init(delta_tool, cache))
  }



  /**
   * Create new HistoryManager class
   * 
   * @param core HistoryCore.
   */
  constructor(private core: HistoryCore<Data, Meta>) { }



  /** Get a detached snapshot of the HistoryInstance */
  snapshot(index?: number): HistoryInstance<Data, Meta> {
    if (index === undefined) index = (this.core.history?.delta.length ?? 0) - 1
    const view = this.core.view(index)

    const promote: HistoryInstance<Data, Meta>['promote'] = () => {
      this.core.set(view.history)
      return this
    }

    const obj = new HistoryManager(view) as unknown as HistoryInstance<Data, Meta>

    const proxy: HistoryInstance<Data, Meta> = new Proxy(
      obj,
      {
        get(target, prop: keyof typeof obj) {
          if (prop === 'promote') return promote
          return target[prop]
        }
      }
    )

    return proxy
  }






  /**
   * Search through history, matching oldest first.
   * 
   * @param fn Function to search for matching meta.
   */
  first(fn: (meta: Meta, index: number) => boolean): HistoryInstance<Data, Meta>
  /**
   * Search through history, matching oldest first.
   * 
   * @param fn Function to search for matching meta.
   * @param throws `false` Set to return undefined when meta not found instead of throwing.
   */
  first(fn: (meta: Meta, index: number) => boolean, throws: false): HistoryInstance<Data, Meta> | undefined
  first(fn: (meta: Meta, index: number) => boolean, throws: boolean): HistoryInstance<Data, Meta> | undefined
  first(fn: (meta: Meta, index: number) => boolean, throws: boolean = true): HistoryInstance<Data, Meta> | undefined {
    if (!this.core.history) throw RangeError('No history to search')

    const i = this.core.history.delta.findIndex(({ meta }, i) => fn(meta, i))

    if (i === -1) {
      if (throws) throw RangeError('No matching entry found')
      return
    }

    return this.snapshot(i + 1)
  }


  /**
   * Search through history, matching most recent first.
   * 
   * @param fn Function to search for matching meta.
   */
  last(fn: (meta: Meta, index: number) => boolean): HistoryInstance<Data, Meta>
  /**
   * Search through history, matching most recent first.
   * 
   * @param fn Function to search for matching meta.
   * @param throws `false` Set to return undefined when meta not found instead of throwing.
   */
  last(fn: (meta: Meta, index: number) => boolean, throws: false): HistoryInstance<Data, Meta> | undefined
  last(fn: (meta: Meta, index: number) => boolean, throws: boolean): HistoryInstance<Data, Meta> | undefined
  last(fn: (meta: Meta, index: number) => boolean, throws: boolean = true): HistoryInstance<Data, Meta> | undefined {
    if (!this.core.history) throw RangeError('No history to search')

    const i = (() => {
      for (let i = this.core.history.delta.length - 1; i >= 0; i--) {
        if (fn(this.core.history.delta[i].meta, i)) return i
      }
      return -1
    })()

    if (i === -1) {
      if (throws) throw RangeError('No matching entry found')
      return
    }

    return this.snapshot(i + 1)
  }



  /**
   * Push new data to history.
   * 
   * @param data Data to add to history.
   */
  push(data: { data: Data, meta: Meta }) {
    return this.core.push(data)
  }

  /**
   * Generates result of push without pushing.
   * 
   * @param data Data to add to history.
   */
  dry_push(data: { data: Data, meta: Meta }) {
    return this.core.dry_push(data)
  }



  /**
   * Updates the history with delta.
   * 
   * @param delta Delta to update history with.
   */
  public update(delta: Delta) {
    return this.core.update(delta)
  }



  /**
   * Updates the history with list of deltas.
   * 
   * @param delta List of deltas to update history with.
   */
  public update_batch(delta: DeltaList) {
    return this.core.update_batch(delta)
  }



  get has_history() {
    return this.core.has_history
  }

  get data() {
    return this.core.data
  }

  get meta() {
    return this.core.meta
  }

  get delta() {
    return this.core.delta
  }

  get changelog() {
    return this.core.changelog
  }

  get revision() {
    return this.core.revision
  }
} 