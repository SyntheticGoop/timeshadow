/// <reference types="@types/jest"/>
import type { DeltaList } from './Changelog.t'
import { BasicDiff } from './DeltaTool'
import { Timeshadow } from './Timeshadow'

const d = BasicDiff

describe('tests', () => {
  const datamodel = [
    { data: { a: 1, b: [2, { c: 3 }] }, meta: { x: 1, a: 1, b: [2, { c: 3 }] } },
    { data: { a: 4, b: [2, { c: 3 }] }, meta: { x: 2, a: 1, b: [5, { c: 3 }] } },
    { data: { a: 4, n: [2, { c: 4 }] }, meta: { x: 3, o: 1, b: [2, { c: 1 }] } },
    { data: { a: 2, n: [2, { c: 3 }] }, meta: { x: 4, o: 1, u: [2, { e: 3 }] } },
    { data: { a: 2, e: [1, { c: 3 }] }, meta: { x: 5, i: 1, u: [2, { e: 3 }] } },
    { data: { a: 2, p: [1, { c: 3 }] }, meta: { x: 6, i: 1, u: [2, { e: 4 }] } },
  ]

  const delta_list: DeltaList = [
    d.create([datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[1].data, datamodel[1].meta], [datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[2].data, datamodel[2].meta], [datamodel[1].data, datamodel[1].meta])!,
    d.create([datamodel[3].data, datamodel[3].meta], [datamodel[2].data, datamodel[2].meta])!,
    d.create([datamodel[4].data, datamodel[4].meta], [datamodel[3].data, datamodel[3].meta])!,
    d.create([datamodel[5].data, datamodel[5].meta], [datamodel[4].data, datamodel[4].meta])!,
  ]

  it('Loads from deltalist', () => {
    expect(Timeshadow.new(d, { delta: delta_list }).delta).toEqual(delta_list)
  })

  it('Loads from initial', () => {
    expect(Timeshadow.init(d, { data: datamodel[0].data, meta: datamodel[0].meta }).commit()).toEqual(delta_list[0])
  })

  it('Loads from both', () => {
    const ts = Timeshadow.follow(d, { data: datamodel[4].data, meta: datamodel[4].meta, delta: delta_list.slice(0, 4) as DeltaList })
    expect(ts.delta).toEqual(delta_list.slice(0, 4))
    expect(ts.data).toEqual(datamodel[4].data)
    expect(ts.meta).toEqual(datamodel[4].meta)
  })

  describe('Update', () => {
    it('Updates when mutated', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m = ts.shadow
      m.data = datamodel[4].data
      m.meta = datamodel[4].meta
      expect(ts.delta).toEqual(delta_list.slice(0, 4))
      m.commit()
      expect(ts.delta).toEqual(delta_list.slice(0, 5))
    })

    it(`Doesn't update if nothing has changed`, () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      expect(ts.delta).toEqual(delta_list.slice(0, 4))
      ts.shadow.commit()
      expect(ts.delta).toEqual(delta_list.slice(0, 4))
    })

    it(`Updates using replace`, () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m = ts.shadow
      expect(ts.delta).toEqual(delta_list.slice(0, 4))
      m.replace(datamodel[4].data, datamodel[4].meta).commit()
      expect(ts.delta).toEqual(delta_list.slice(0, 5))
    })

    it(`Updates using modify`, () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m = ts.shadow
      expect(ts.delta).toEqual(delta_list.slice(0, 4))
      m.modify(() => datamodel[4].data, datamodel[4].meta).commit()
      expect(ts.delta).toEqual(delta_list.slice(0, 5))
    })

    it('Detects a conflict', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m1 = ts.shadow
      const m2 = ts.shadow
      m1.replace(datamodel[4].data, datamodel[4].meta).commit()
      expect(m2.conflict).toBeTruthy()
      expect(m1.conflict).toBeFalsy()
    })

    it('Resolves lazy shadow to correct history', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m1 = ts.shadow
      const m2 = ts.lazy_shadow
      m1.replace(datamodel[4].data, datamodel[4].meta).commit()

      expect(m1.conflict).toBeFalsy()
      const snap = m2()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)

      m1.replace(datamodel[5].data, datamodel[5].meta).commit()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)
    })

    it('Resolves multiple lazy shadows to same initial initialized data', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m1 = ts.shadow
      const m2 = ts.lazy_shadow
      m1.replace(datamodel[4].data, datamodel[4].meta).commit()

      const snap = m2()
      snap.replace(datamodel[5].data, datamodel[5].meta)
      expect(snap.data).toEqual(datamodel[5].data)
      expect(m2().data).toEqual(datamodel[5].data)
    })

    it('Is able to transverse back to the correct history with last', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m1 = ts.shadow
      m1.replace(datamodel[4].data, datamodel[4].meta).commit()

      expect(m1.conflict).toBeFalsy()
      const snap = ts.last(x => x.x === 4).shadow()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)

      m1.replace(datamodel[5].data, datamodel[5].meta).commit()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)
    })

    it('Is able to transverse back to the correct history with first', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const m1 = ts.shadow
      m1.replace(datamodel[4].data, datamodel[4].meta).commit()

      expect(m1.conflict).toBeFalsy()
      const snap = ts.first(x => x.x === 4).shadow()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)

      m1.replace(datamodel[5].data, datamodel[5].meta).commit()
      expect(snap.conflict).toBeTruthy()
      expect(snap.data).toEqual(datamodel[3].data)
    })

    it('Is able to modify transversed history with last', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const snap = ts.last(x => x.x === 4).shadow()
      expect(snap.data).not.toEqual(datamodel[4].data)
      snap.replace(datamodel[4].data, datamodel[4].meta).commit()
      expect(snap.data).toEqual(datamodel[4].data)
    })

    it('Is able to modify transversed history with first', () => {
      const ts = Timeshadow.new(d, { delta: delta_list.slice(0, 4) as DeltaList })
      const snap = ts.first(x => x.x === 4).shadow()
      expect(snap.data).not.toEqual(datamodel[4].data)
      snap.replace(datamodel[4].data, datamodel[4].meta).commit()
      expect(snap.data).toEqual(datamodel[4].data)
    })
  })
})