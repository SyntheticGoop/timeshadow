import { immutalize, remutalize } from 'immutalize'
import type { Delta } from 'jsondiffpatch'
import { equals } from 'ramda'
import type { Coredata, DefinedCoredata, DeltaList } from './Changelog.t'
import type { DeltaTool } from './DeltaTool'
import { HistoryManager } from './HistoryManager'
import { transverse } from './transverse'


/**
 * Stores versions of an object, allowing transversal through previous versions.
 */
export class Timeshadow<Data extends Coredata, Meta extends DefinedCoredata> {
  /** History Manager */
  public history: HistoryManager<Data, Meta>


  /**
   * Creates new Diff Data Object.
   * 
   * Data and Meta provided will be ignored.
   * 
   * @param delta_tool The delta algorithm.
   * @param data The diff data.
   * @param cache The size of the LRU cache.
   */
  static new<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, data: { delta: DeltaList, data?: Data, meta?: Meta }, cache: number = 10): Timeshadow<Data, Meta> {
    return new Timeshadow<Data, Meta>(delta_tool, data.delta, cache)
  }



  /**
   * Create new Diff Data Object and set data as shadow.
   * 
   * Delta will be ignored.
   * 
   * @param delta_tool The delta algorithm.
   * @param data The data to initialize with.
   * @param cache The size of the LRU cache.
   */
  static init<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, data: { data: Data, meta: Meta, delta?: DeltaList }, cache: number = 10): Timeshadow<Data, Meta>['shadow'] {
    return new Timeshadow(delta_tool, undefined, cache)
      .shadow
      .replace(data.data, data.meta)
  }

  /**
   * Create new Diff Data Object and set data as shadow.
   * 
   * @param delta_tool The delta algorithm.
   * @param data The data to initialize with, and the delta to begin from.
   * @param cache The size of the LRU cache.
   */
  static follow<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, data: { data: Data, meta: Meta, delta: DeltaList }, cache: number = 10): Timeshadow<Data, Meta>['shadow'] {
    return new Timeshadow(delta_tool, data.delta, cache)
      .shadow
      .replace(data.data, data.meta)
  }



  /**
   * Create new Timeshadow.
   * 
   * @param data Data to initialize with.
   * @param cache Size of history LRU cache.
   */
  constructor(private delta_tool: DeltaTool<Data, Meta>, delta?: DeltaList, cache: number = 10) {
    if (delta) {
      this.history = HistoryManager.new(delta_tool, delta, cache)
    } else {
      this.history = HistoryManager.init(delta_tool, cache)
    }
  }


  /**
   * Search from first to last for matching history.
   * 
   * @param fn Search Function
   */
  first(fn: (meta: Meta, index: number) => boolean): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> }
  /**
   * Search from first to last for matching history.
   * 
   * @param fn Search Function
   * @param throws Don't throw an error if a match cannot be found.
   */
  first(fn: (meta: Meta, index: number) => boolean, throws: false): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined
  first(fn: (meta: Meta, index: number) => boolean, throws: boolean = true): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined {
    const base = this
    const snapshot = base.history.first(fn, throws)

    const proxy: typeof snapshot & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined = snapshot ? new Proxy(
      snapshot as typeof snapshot & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> },
      {
        get(target, prop: keyof typeof snapshot) {
          if (prop === 'promote') {
            return function () {
              const promoted = target.promote()
              base.history = promoted
              return base
            }
          }
          if (prop as any === 'shadow') {
            return function () {
              const source = snapshot.has_history ? {
                data: remutalize(snapshot.data as any),
                meta: remutalize(snapshot.meta as any),
              } : {
                  data: undefined as any,
                  meta: undefined as any,
                }

              return base._shadow(snapshot, source)
            }
          }
          return target[prop]
        }
      }
    ) : undefined

    return proxy
  }

  /**
   * Search from last to first for matching history.
   * 
   * @param fn Search Function
   */
  last(fn: (meta: Meta, index: number) => boolean): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> }
  /**
   * Search from last to first for matching history.
   * 
   * @param fn Search Function
   * @param throws Don't throw an error if a match cannot be found.
   */
  last(fn: (meta: Meta, index: number) => boolean, throws: false): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined
  last(fn: (meta: Meta, index: number) => boolean, throws: boolean = true): HistoryManager<Data, Meta> & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined {
    const base = this
    const snapshot = base.history.last(fn, throws)

    const proxy: typeof snapshot & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> } | undefined = snapshot ? new Proxy(
      snapshot as typeof snapshot & { promote: () => Timeshadow<Data, Meta>, shadow: () => Shadow<Timeshadow<Data, Meta>> },
      {
        get(target, prop: keyof typeof snapshot) {
          if (prop === 'promote') {
            return function () {
              const promoted = target.promote()
              base.history = promoted
              return base
            }
          }
          if (prop as any === 'shadow') {
            return function () {
              const source = snapshot.has_history ? {
                data: remutalize(snapshot.data as any),
                meta: remutalize(snapshot.meta as any),
              } : {
                  data: undefined as any,
                  meta: undefined as any,
                }

              return base._shadow(snapshot, source)
            }
          }
          return target[prop]
        }
      }
    ) : undefined

    return proxy
  }

  /**
   * Creates a copy which shares all data with this object except for the `data` and `meta` properties.
   * 
   * An additional property `conflict` will return `true` when history has changed while using shadow.
   * 
   * Data will be updated to the root object when commit is called.
   * 
   * @param history Provide the history instance to shadow
   * @param source Provide source data to use (can be used to allow data sharing)
   */
  private _shadow(history: HistoryManager<Data, Meta>, source: { data: HistoryManager<Data, Meta>['data'], meta: HistoryManager<Data, Meta>['meta'] } | { data: undefined, meta: undefined }): Shadow<Timeshadow<Data, Meta>> {
    const root = this as Shadow<Timeshadow<Data, Meta>> & Timeshadow<Data, Meta>

    let rev = root.history.revision


    const p: Shadow<Timeshadow<Data, Meta>> = new Proxy(
      root,
      {
        get(target, prop: keyof typeof root) {
          if (prop === 'data') {
            return source.data
          }

          else if (prop === 'meta') {
            return source.meta
          }

          else if (prop === 'commit') {
            return () => {
              // @ts-expect-error
              const change = target.history.push(source)
              rev = history.revision
              return change
            }
          }

          else if (prop === 'dry_commit') {
            return () => {
              // @ts-expect-error
              return target.history.dry_push(source)
            }
          }

          else if (prop === 'modify') {
            return (update: any, meta: any, force: any) => {
              modify(source, update, meta, force)
              return p
            }
          }

          else if (prop === 'partial') {
            return (data: any, meta: any, force: any) => {
              partial(source, data, meta, force)
              return p
            }
          }

          else if (prop === 'replace') {
            return (data: any, meta: any, force: any) => {
              replace(source, data, meta, force)
              return p
            }
          }

          else if (prop === 'conflict') {
            return rev !== history.revision
          }

          else if (prop === 'shadow') {
            const s = target[prop]
            // @ts-expect-error
            s.data = source.data
            // @ts-expect-error
            s.meta = source.meta

            return s
          }

          else if (prop === 'root') {
            return root
          }

          else {
            return target[prop]
          }
        },
        set(_target, prop: keyof typeof root, val) {
          if (prop === 'data') {
            source.data = val
          }
          else if (prop === 'meta') {
            source.meta = val
          }
          return true
        },
      }
    )

    return p
  }




  /**
   * Data provided by the resulting function will be a snapshot of data from when the property was first called, not when
   * then resulting function is invoked.
   * 
   * Creates a lazy copy which shares all data with this object except for the `data` and `meta` properties.
   * 
   * An additional property `conflict` will return `true` when history has changed while using shadow.
   * 
   * Data will be updated to the root object when commit is called.
   * 
   * Multiple lazy shadows invoked from the same getted property will point to the same root data, but will not reflect the changes of each other.
   */
  get lazy_shadow() {
    const root = this as Shadow<Timeshadow<Data, Meta>> & Timeshadow<Data, Meta>
    const revision = this.history.revision
    let unset = true

    let history = root.history

    let source: {
      data: any,
      meta: any,
    } = {
      data: undefined,
      meta: undefined,
    }

    return () => {
      history = history.snapshot(revision)

      if (unset) {
        unset = false
        source = history.has_history ? {
          data: remutalize(history.data as any),
          meta: remutalize(history.meta as any),
        } : {
            data: undefined as any,
            meta: undefined as any,
          }
      }

      return root._shadow(history, source)
    }
  }

  /**
   * Creates a copy which shares all data with this object except for the `data` and `meta` properties.
   * 
   * An additional property `conflict` will return `true` when history has changed while using shadow.
   * 
   * Data will be updated to the root object when commit is called.
   */
  get shadow(): Shadow<Timeshadow<Data, Meta>> {
    const source = this.history.has_history ? {
      data: remutalize(this.history.data as any),
      meta: remutalize(this.history.meta as any),
    } : {
        data: undefined as any,
        meta: undefined as any,
      }
    return this._shadow(this.history, source)
  }



  /**
   * Immutable current data.
   * In order to edit, you must use a shadow.
   */
  get data() {
    return this.changelog.data
  }


  /**
   * Immutable current meta.
   * In order to edit, you must use a shadow.
   */
  get meta() {
    return this.changelog.meta
  }



  /**
   * Immutable full uncompressed history.
   */
  get full() {
    return Array.from(transverse(this.delta_tool, this.history.delta))
  }



  /**
   * Immutable delta.
   */
  get delta() {
    return this.changelog.delta
  }

  /**
   * Immutable changelog
   */
  get changelog() {
    return immutalize(this.history.changelog)
  }
}

type TimeData<T> = T extends Timeshadow<infer Data, any> ? Data : never
type TimeMeta<T> = T extends Timeshadow<any, infer Meta> ? Meta : never

type Shadow<T extends Timeshadow<any, any>> = Omit<T, 'data' | 'meta'> & {

  /** Source Timeshadow */
  root: T



  /** Mutable data */
  data: TimeData<T>



  /** Mutable meta */
  meta: TimeMeta<T>



  /** Returns `true` when the history has changed from when last shadowed */
  conflict: boolean



  /**
   * Commits any active changes
   */
  commit(): Delta | undefined



  /**
   * Get commit changelog without commiting
   */
  dry_commit(): Delta | undefined



  /**
   * Set a shadow as the main instance
   */
  promote(): T


  /**
   * Updates current live data with replace.
   * 
   * @param data Data to update with.
   * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
   * @param force `Optional` Forces a transaction even if there is no change.
   */
  replace(data: TimeData<T>, meta: TimeMeta<T>, force?: true): Shadow<T>



  /**
   * Updates current live data with merge.
   * 
   * @param data Data to update with.
   * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
   * @param force `Optional` Forces a transaction even if there is no change.
   */
  partial(data: Partial<TimeData<T>>, meta: TimeMeta<T>, force?: true): Shadow<T>



  /**
   * Updates current live data with query function.
   * 
   * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
   * @param force `Optional` Force a transaction even if there is no change.
   */
  modify(update: (data: TimeData<T>) => TimeData<T>, meta: TimeMeta<T>, force?: true): Shadow<T>
}





/**
 * Updates current live data with replace.
 * 
 * @param source Source data.
 * @param data Data to update with.
 * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
 * @param force `Optional` Forces a transaction even if there is no change.
 */
function replace<Data, Meta>(source: { data: Data, meta: Meta }, data: Data, meta: Meta, force?: true) {
  if (force || !equals(data, source.data)) {
    source.data = data
    source.meta = meta
  }
}



/**
 * Updates current live data with merge.
 * 
 * @param source Source data.
 * @param data Data to update with.
 * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
 * @param force `Optional` Forces a transaction even if there is no change.
 */
function partial<Data, Meta>(source: { data: Data, meta: Meta }, data: Partial<Data>, meta: Meta, force?: true) {
  const new_data: Data = Object.assign({}, source.data, data)
  const new_meta: Meta = Object.assign({}, source.meta, meta)
  replace(source, new_data, new_meta, force)
}



/**
 * Updates current live data with query function.
 * 
 * @param source Source data.
 * @param meta Meta to include as part of transaction. Metadata does not factor as 'changed' data.
 * @param force `Optional` Force a transaction even if there is no change.
 */
function modify<Data, Meta>(source: { data: Data, meta: Meta }, update: (data: Data) => Data, meta: Meta, force?: true) {
  const new_data = update(remutalize(source.data))
  replace(source, new_data, meta, force)
}