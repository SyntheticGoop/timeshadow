import 'ts-jest'
import type { DeltaList } from './Changelog.t'
import { BasicDiff } from './DeltaTool'
import { transverse } from './transverse'

const d = BasicDiff

describe('tests', () => {
  const datamodel = [
    { data: { a: 1, b: [2, { c: 3 }] }, meta: { a: 1, b: [2, { c: 3 }] } },
    { data: { a: 4, b: [2, { c: 3 }] }, meta: { a: 1, b: [5, { c: 3 }] } },
    { data: { a: 4, n: [2, { c: 4 }] }, meta: { o: 1, b: [2, { c: 1 }] } },
    { data: { a: 2, n: [2, { c: 3 }] }, meta: { o: 1, u: [2, { e: 3 }] } },
    { data: { a: 2, e: [1, { c: 3 }] }, meta: { i: 1, u: [2, { e: 3 }] } },
  ]

  const delta_list: DeltaList = [
    d.create([datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[1].data, datamodel[1].meta], [datamodel[0].data, datamodel[0].meta])!,
    d.create([datamodel[2].data, datamodel[2].meta], [datamodel[1].data, datamodel[1].meta])!,
    d.create([datamodel[3].data, datamodel[3].meta], [datamodel[2].data, datamodel[2].meta])!,
    d.create([datamodel[4].data, datamodel[4].meta], [datamodel[3].data, datamodel[3].meta])!,
  ]

  it('Correctly restores', () => expect(Array.from(transverse(d, delta_list)).map(({ delta, ...x }) => x)).toEqual(datamodel))

  it('Correctly restores partial', () => expect(Array.from(transverse(d, delta_list.slice(2) as DeltaList, { data: datamodel[1].data, meta: datamodel[1].meta })).map(({ delta, ...x }) => x)).toEqual(datamodel.slice(2)))
})