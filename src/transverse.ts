import { immutalize } from 'immutalize';
import type { Delta } from 'jsondiffpatch';
import { ChangeList, Coredata, DefinedCoredata, DeltaList, is_delta_list } from './Changelog.t';
import type { DeltaTool } from './DeltaTool';

/**
 * Loop through `DeltaList`
 * 
 * @param delta_tool `DeltaTool` differ
 * @param delta `DeltaList` to loop through.
 * @param base Change list to start looping from.
 */
export function* transverse<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, delta: DeltaList | Delta[], base?: { data: Data, meta: Meta }) {
  if (!is_delta_list(delta)) throw Error('Delta does not have at least 1 item.')
  let current: ChangeList<Data, Meta> | undefined = base ? [base.data, base.meta] : undefined
  for (const d of delta) {
    current = delta_tool.revert(d, current)

    yield immutalize({ data: current![0], meta: current![1], delta: d })
  }

  return
}

/**
 * Loop through `DeltaList` and return last item.
 * 
 * @param delta_tool `DeltaTool` differ
 * @param delta `DeltaList` to loop through.
 * @param base Change list to start looping from.
 */
export function transverse_last<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, delta: DeltaList | Delta[], base?: { data: Data, meta: Meta }) {
  if (!is_delta_list(delta)) throw Error('Delta does not have at least 1 item.')
  let current: { data: Data, meta: Meta, delta: Delta } | undefined = undefined

  let active: ChangeList<Data, Meta> | undefined = base ? [base.data, base.meta] : undefined

  for (const d of delta) {
    active = delta_tool.revert(d, active)
    current = { data: active![0], meta: active![1], delta: d }
  }

  if (!current) throw Error('No data to get last of')
  return immutalize(current)
}
/**
 * Loop through `DeltaList` and return delta history.
 * 
 * @param delta_tool `DeltaTool` differ
 * @param delta `DeltaList` to loop through.
 * @param base Change list to start looping from.
 */
export function crush_deltas<Data extends Coredata, Meta extends DefinedCoredata>(delta_tool: DeltaTool<Data, Meta>, delta: DeltaList | Delta[], base?: { data: Data, meta: Meta }) {
  if (!is_delta_list(delta)) throw Error('Delta does not have at least 1 item.')
  let current: { data: Data, meta: Meta, delta: Delta } | undefined = undefined
  const history: Array<{ meta: Meta, delta: Delta }> = []

  let active: ChangeList<Data, Meta> | undefined = base ? [base.data, base.meta] : undefined

  for (const d of delta) {
    active = delta_tool.revert(d, active)
    if (current) history.push({ delta: current.delta, meta: current.meta })
    current = { data: active![0], meta: active![1], delta: d }
  }

  if (!current) throw Error('No data to get last of')
  return {
    active: immutalize(current),
    history: immutalize(history),
  }
}